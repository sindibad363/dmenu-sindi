dmenu - dynamic menu
====================
dmenu is an efficient dynamic menu for X.

Installation
------------
```
git clone https://gitlab.com/sindibad363/dmenu-sindi.git
cd dmenu-sindi
sudo/doas make clean install
```
